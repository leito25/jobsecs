﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class testRenderer : MonoBehaviour
{
    [SerializeField] private Mesh t_mesh;
    [SerializeField] private Material t_material;


    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.AddComponent<MeshRenderer>().material = t_material;
        this.gameObject.AddComponent<MeshFilter>().mesh = t_mesh;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
