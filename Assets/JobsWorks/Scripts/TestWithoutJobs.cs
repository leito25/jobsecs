﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Mathematics;//maths library

public class TestWithoutJobs : MonoBehaviour
{
    [SerializeField] private Transform pfNPC;
    private List<NPCChar> npcList;

    public class NPCChar
    {
        public Transform transformacion;
        public float moveX;
    }


    [SerializeField] private bool useJobs = false;

    public Text opTime;
    public Button jobSwitch;

    public void activateJobs()
    {
        useJobs = !useJobs;
        jobSwitch.GetComponentInChildren<Text>().text = "jobs: " + useJobs;
    }

    private void Start()
    {

        npcList = new List<NPCChar>();
        for (int i = 0; i < 1000; i++)
        {
            Transform npcTransform = Instantiate(pfNPC, new Vector3(UnityEngine.Random.Range(-6f, 6f), UnityEngine.Random.Range(-14f, 14f)), Quaternion.identity);
            npcList.Add(new NPCChar
            {
                transformacion = npcTransform,
                moveX = UnityEngine.Random.Range(1f, 2f)
            });
        }
    }

    private void Update()
    {

        Debug.Log(">>CLASIC");
        //Classic way to instantiate
        foreach (NPCChar npc in npcList)
        {
            //moving the npc
            npc.transformacion.position += new Vector3(npc.moveX * Time.deltaTime, 0);
            if (npc.transformacion.position.x > 8f)
            {
                npc.moveX = -math.abs(npc.moveX);
                /*Vector3 theScale = npc.transformacion.localScale;
                theScale.x *= -1;
                npc.transformacion.localScale = theScale;*/

            }
            if (npc.transformacion.position.x < -8f)
            {
                npc.moveX = +math.abs(npc.moveX);
                /*Vector3 theScale = npc.transformacion.localScale;
                theScale.x *= -1;
                npc.transformacion.localScale = theScale;*/
            }
        }
    }
}
