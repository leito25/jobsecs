﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public struct ecs_LevelComponent : IComponentData
{
    public float level;
}
