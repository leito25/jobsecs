﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Collections;
using Unity.Rendering;
using Unity.Mathematics;

public class testing_ecs : MonoBehaviour
{
    [SerializeField] private Mesh t_mesh;
    [SerializeField] private Material t_material;

    // Start is called before the first frame update
    void Start()
    {
        EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        //the component could have many diferent properties using archetypes
        EntityArchetype entityArchWithRender = entityManager.CreateArchetype(
            typeof(ecs_LevelComponent),
            typeof(Translation),
            typeof(RenderMesh),
            typeof(RenderBounds),
            typeof(LocalToWorld),
            typeof(ecs_MoveSpeedComponent)//componente para la velocidad
            );

        //using native array to passing a lot of information
        NativeArray<Entity> entityArray = new NativeArray<Entity>(10000, Allocator.Temp);//number of entities-temporalallocator
        entityManager.CreateEntity(entityArchWithRender, entityArray);

        //entity creation based on the nativearray
        for(int i = 0; i < entityArray.Length; i++)
        {
            Entity entity = entityArray[i];

            entityManager.SetComponentData(entity,
                new ecs_LevelComponent
                {
                    level = UnityEngine.Random.Range(10, 50)
                });
            entityManager.SetComponentData(entity,
                new ecs_MoveSpeedComponent
                {
                    speed = UnityEngine.Random.Range(1f, 2f)
                });//spped component
            entityManager.SetComponentData(entity,
                new Translation {
                    Value = new float3(UnityEngine.Random.Range(-2f, 2f), UnityEngine.Random.Range(-5f, 5f), 0)
                });

            //the render data is shared component data
            entityManager.SetSharedComponentData(entity, new RenderMesh
            {
                mesh = t_mesh,
                material = t_material,
            });

        }

        //dispose the array
        entityArray.Dispose();
        
    }
}
