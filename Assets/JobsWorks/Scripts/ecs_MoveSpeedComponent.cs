﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public struct ecs_MoveSpeedComponent : IComponentData
{
    public float speed;
}
