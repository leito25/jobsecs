﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class ecs_LevelUPSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref ecs_LevelComponent m_levelComponent) =>
        {
            m_levelComponent.level += 1f * Time.DeltaTime;

            //using another behaviour diferent to the component system as a debug.lo
            //decrease the performance

            //Debug.Log(m_levelComponent.level);
        });
    }
}
