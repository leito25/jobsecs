﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Mathematics;//maths library
using Unity.Jobs;//jobs
using Unity.Collections;
using Unity.Burst;
using UnityEngine.UI;

public class JobsTextureParallel : MonoBehaviour
{
    //Lista de arreglos con los datos de los cortes de la img inicial
    [SerializeField] public List<float[]> pxData_List = new List<float[]>();
    [SerializeField] public List<float> res_List = new List<float>();

    public float total_R;

    public int pxCounterCut = 0;

    public float w;
    public float h;

    //imagen de base
    public Texture2D m_texture2D;

    //recortes de la imagen base
    public Texture2D m_text1;
    public Texture2D m_text2;
    public Texture2D m_text3;
    public Texture2D m_text4;

    public int mm_text_size;//tamaño de pixeles de cada recorte

    public List<Texture2D> text2D_List;//lista de las texturas

    //lista con los totales de rojo de cada recorte
    public List<float> RED_List = new List<float>();

    //Colores para la asignación de pixees para cada recorte
    Color tempColor1;
    Color tempColor2;
    Color tempColor3;
    Color tempColor4;


    // Start
    void Start()
    {
        GetComponent<Renderer>().material.mainTexture = m_texture2D;
        cutPieces();//recortar la imagen base y asignar los recortes a unas nuevas texturas
        fillArrays();//rellenar los arrays con la info de los pixeles
        calculateSum();
    }

    public void fillArrays()
    {
        pxData_List.Clear();
        for (int i = 0; i < 4; i++)//4 means each separate picture
        {
            pxData_List.Add(getDataFromImage(i).ToArray());
        }
    }

    public void cutPieces()
    {
        text2D_List.Clear();
        pxCounterCut = 0;

        m_text1 = new Texture2D(m_texture2D.width / 2, m_texture2D.height / 2, TextureFormat.RGBA32, false);
        m_text2 = new Texture2D(m_texture2D.width / 2, m_texture2D.height / 2, TextureFormat.RGBA32, false);
        m_text3 = new Texture2D(m_texture2D.width / 2, m_texture2D.height / 2, TextureFormat.RGBA32, false);
        m_text4 = new Texture2D(m_texture2D.width / 2, m_texture2D.height / 2, TextureFormat.RGBA32, false);

        //iteración asignación de valores
        for (int y = 0; y < m_text1.height; y++)
        {
            for (int x = 0; x < m_text1.width; x++)
            {
                tempColor1 = m_texture2D.GetPixel(x, y);
                m_text1.SetPixel(x, y, tempColor1);

                tempColor2 = m_texture2D.GetPixel(x + m_text1.width, y);
                m_text2.SetPixel(x, y, tempColor2);

                tempColor3 = m_texture2D.GetPixel(x, y + m_text1.height);
                m_text3.SetPixel(x, y, tempColor3);

                tempColor4 = m_texture2D.GetPixel(x + m_text1.width, y + m_text1.height);
                m_text4.SetPixel(x, y, tempColor4);

                pxCounterCut++;//comprobacion del numero de pixeles

            }
        }

        //aplicación de pixeles a las nuevas texturas
        m_text1.Apply();
        m_text2.Apply();
        m_text3.Apply();
        m_text4.Apply();

        //se agregan las nuevas texturas a una lista de texturas
        text2D_List.Add(m_text1);
        text2D_List.Add(m_text2);
        text2D_List.Add(m_text3);
        text2D_List.Add(m_text4);

        //variable para ver el tamaño en número de pixeles
        mm_text_size = m_text1.width * m_text1.height;
        w = m_text1.width;
        h = m_text1.height;
    }

    //metodo para recoger el valor del canal Rojo en cada pixel
    public List<float> getDataFromImage(int indexVal)
    {

        List<float> r_color_data = new List<float>();

        for (int i = 0; i < text2D_List.Count; i++)//4 means each separate picture
        {
            //get red channel float
            for (int x = 0; x < text2D_List[indexVal].width; x++)
            {
                for (int y = 0; y < text2D_List[indexVal].height; y++)
                {
                    r_color_data.Add(text2D_List[indexVal].GetPixel(x, y).r);
                }
            }
        }

        return r_color_data;

    }

    //calcular la suma de cada dato de rojo en cada pixel
    //por un lado sin jobs y de otro con jobs
    public void calculateSum()
    {
        total_R = 0;
        RED_List.Clear();
        
        Debug.Log("Using jobs");
        
        GetComponent<Renderer>().material.mainTexture = m_texture2D;

            cutPieces();
            fillArrays();

            //Creación de cada job calculando los rojos de cada textura de recorte
            foreach (float[] xpdata in pxData_List)
            {
                //creación de lo natve array, que despues se rellenaran con datos de pixeles
                NativeArray<float> nv_pxData = new NativeArray<float>(xpdata.Length, Allocator.TempJob);
                NativeArray<float> nv_result = new NativeArray<float>(1, Allocator.TempJob);

                //paso de datos de la imagen recorte al nativeArray
                for (int i = 0; i < xpdata.Length; i++)//m_text_size es igual a xpdata.w * xpdata.h
                {
                    nv_pxData[i] = xpdata[i];
                }

                //creación de job y traspaso de datos
                SumJobSimple mySimpleJob = new SumJobSimple
                {
                    pxData = nv_pxData,//datos de colores
                    result = nv_result//resultado
                };

                //execution
                JobHandle job_H = mySimpleJob.Schedule();
                job_H.Complete();

                //lista de resultados
                RED_List.Add(mySimpleJob.result[0]);
                total_R += mySimpleJob.result[0];

                //liberar los native array
                nv_pxData.Dispose();
                nv_result.Dispose();
            }

    }
}

[BurstCompile]
public struct SumJobSimple : IJob
{
    [ReadOnly]
    public NativeArray<float> pxData;
    public NativeArray<float> result;

    public void Execute()
    {
        //Suma de los valores de rojo en cada pixel
        for (int i = 0; i < pxData.Length; i++)
        {
            result[0] += pxData[i];
        }
    }
}
