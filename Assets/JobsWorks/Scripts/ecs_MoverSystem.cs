﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class ecs_MoverSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref Translation translation, ref ecs_MoveSpeedComponent movercomponent) =>
        {
            translation.Value.x += movercomponent.speed * Time.DeltaTime;
            if (translation.Value.x > 3f)
            {
                movercomponent.speed = -math.abs(movercomponent.speed);
            }
            if (translation.Value.x < -3f)
            {
                movercomponent.speed = +math.abs(movercomponent.speed);
            }
        });
    }
}
