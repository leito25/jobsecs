﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Mathematics;//maths library
using Unity.Jobs;//jobs
using Unity.Collections;
using Unity.Burst;
using UnityEngine.UI;

public class Testingg : MonoBehaviour
{
    [SerializeField] private Transform pfNPC;
    private List<NPCChar> npcList;

    public class NPCChar
    {
        public Transform transformacion;
        public float moveX;
    }


    [SerializeField] private bool useJobs = false;

    public Text opTime;
    public Button jobSwitch;
    
    public void activateJobs()
    {
        useJobs = !useJobs;
        jobSwitch.GetComponentInChildren<Text>().text = "jobs: " + useJobs;
    }

    int numberOfObjs = 1000;

    private void Start()
    {
        jobSwitch.GetComponentInChildren<Text>().text = "jobs: " + useJobs;

        //instanciando todos los spritecitos en una lista para despues mandarla a procesamiento
        npcList = new List<NPCChar>();
        for (int i = 0; i < numberOfObjs; i++)
        {
            Transform npcTransform = Instantiate(pfNPC, new Vector3(UnityEngine.Random.Range(-6f, 6f), UnityEngine.Random.Range(-14f, 14f)), Quaternion.identity);
            npcList.Add(new NPCChar
            {
                transformacion = npcTransform,
                moveX = UnityEngine.Random.Range(1f, 2f)
            });
        }
    }

    



    private void Update()
    {
        //taking the time
        float startTime = Time.realtimeSinceStartup;

        
        //PARALLEL JOB TEST
        if (useJobs)
        {
            Debug.Log(">>JOBS");
            //parallel jobs implementation
            //crear los arrays con el tamaño dela cantidad de npcs
            //NativeArray<float3> x_positionArray = new NativeArray<float3>(npcList.Count, Allocator.TempJob);
            NativeArray<float> x_moveX_Array = new NativeArray<float>(npcList.Count, Allocator.TempJob);
            TransformAccessArray x_transformAccessArray = new TransformAccessArray(npcList.Count);


            //asignar a cada npc las propiedades
            for (int i = 0; i < npcList.Count; i++)
            {
                //x_positionArray[i] = npcList[i].transformacion.position;
                x_moveX_Array[i] = npcList[i].moveX;
                x_transformAccessArray.Add(npcList[i].transformacion);
            }

            //passing data to the parallel job
            PrettyToughParallelJob reallyTPJob = new PrettyToughParallelJob
            {
                deltaTime = Time.deltaTime,
                //positionArray = x_positionArray,
                moveX_Array = x_moveX_Array,
            };

            //run the jobs
            JobHandle job_hdl  = reallyTPJob.Schedule(x_transformAccessArray);//number of process by each job
            job_hdl.Complete();

            //and finally assing the resulting values to the original values
            for(int i = 0; i < npcList.Count; i++)
            {
                //npcList[i].transformacion.position = x_positionArray[i];
                npcList[i].moveX = x_moveX_Array[i];
            }

            //clear memory - dispose arrays
            x_transformAccessArray.Dispose();
            x_moveX_Array.Dispose();
        }
        else
        {
            Debug.Log(">>CLASIC");
            //Classic way to instantiate
            foreach (NPCChar npc in npcList)
            {
                //moving the npc
                npc.transformacion.position += new Vector3(npc.moveX * Time.deltaTime, 0);
                if (npc.transformacion.position.x > 8f)
                {
                    npc.moveX = -math.abs(npc.moveX);
                    /*Vector3 theScale = npc.transformacion.localScale;
                    theScale.x *= -1;
                    npc.transformacion.localScale = theScale;*/

                }
                if (npc.transformacion.position.x < -8f)
                {
                    npc.moveX = +math.abs(npc.moveX);
                    /*Vector3 theScale = npc.transformacion.localScale;
                    theScale.x *= -1;
                    npc.transformacion.localScale = theScale;*/
                }
                float value = 0f;
                for (int i = 0; i < numberOfObjs; i++)
                {
                    value = math.exp10(math.sqrt(value));
                }
            }
        }
        opTime.text = ((Time.realtimeSinceStartup - startTime) * 1000f) + "ms";
        Debug.Log(((Time.realtimeSinceStartup - startTime) * 1000f) + "ms");
    }
}

//struct using parallel
public struct PrettyToughParallelJob : IJobParallelForTransform
{
    //As aprallel job, cant it be used the unityengine functions as delta time

    //public float3 m_position;turn to an array because there gonna be many objs
    //public NativeArray<float3> positionArray;
    //public float m_moveX;
    public NativeArray<float> moveX_Array;
    [ReadOnly] public float deltaTime;

    public void Execute(int index, TransformAccess transform)//-->the index is used 'cause are many multiple jobs
    {
        //Complex task into the execute job method
        //moving the npc
        transform.position += new Vector3(moveX_Array[index] * deltaTime, 0, 0f);
        if(transform.position.x > 8f)
        {
            moveX_Array[index] = -math.abs(moveX_Array[index]);
        }
        if (transform.position.x < -8f)
        {
            moveX_Array[index] = +math.abs(moveX_Array[index]);
        }


        //Complex task into the execute job method
        float value = 0f;
        for (int i = 0; i < 1000; i++)
        {
            value = math.exp10(math.sqrt(value));
        }
    }
}


